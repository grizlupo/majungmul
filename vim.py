#!/usr/bin/python3

import os
import subprocess
from urllib.request import urlopen

def ensure_dir(path):
    if not os.path.isdir(path):
        os.mkdir(path)

def download_file(url, path):
    f = urlopen(url, timeout=3)
    with open(path, 'wb') as fout:
        fout.write(f.read())

def curl(*args):
    subprocess.call(('curl',) + args)

def git(*args):
    subprocess.call(('git',) + args)

def install_pathogen(prefix='/etc/vim/'):
    autoload = os.path.join(prefix, 'autoload')
    ensure_dir(autoload)
    from_ = 'https://tpo.pe/pathogen.vim'
    to_ = os.path.join(autoload, 'pathogen.vim')
    #download_file(from_, to_)
    curl('-Lo', to_, from_)
    ensure_dir(os.path.join(prefix, 'bundle'))

def install_nim():
    bundle_path = os.path.join('/etc/vim/bundle', 'nim')
    git('clone', 'git://github.com/zah/nimrod.vim.git', bundle_path)

if __name__ == '__main__':
    install_pathogen()
    install_nim()
